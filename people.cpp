
#include "people.h"


People::People():
	step_count(0),
	greeting(GREETING_STRING) {
}

int People::Walk() {
	return ++step_count;
}

void People::Greet() {
	std::cout << greeting << " i made "<< step_count << " steps" << std::endl;
}

int People::GetStepCount() {
	return step_count;
}
