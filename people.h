
#ifndef PEOPLE_H_
#define PEOPLE_H_

#include <iostream>
#include <string>

#define GREETING_STRING "Ciao!"

class People {

public:
	People();

	int Walk();

	void Greet();

	int GetStepCount();

private:
	int step_count;

	std::string greeting;
};


#endif // PEOPLE_H_
