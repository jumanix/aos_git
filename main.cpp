#include <iostream>

#include "people.h"

int main(int argc, const char *argv[])
{
	std::cout << "Hello, walking man!" << std::endl;
	People joe;

	for (int j=0; j<10; ++j) {
		joe.Walk();
	}
	joe.Greet();

	return 0;
}

